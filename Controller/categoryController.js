import categoryModel from "../models/categoryModel.js";
import message from "../helper/messages.js";
import httpCode from "../helper/httpCodes.js";
import services from "../helper/index.js";
const send = services.setResponse;
import { ObjectId } from "mongodb";

class categoryController {
  //render ejs files
  static categoryListAndDeleteView = async (req, res) => {
    res.render('categoryListAndDelete')
  }
  static addCategoryView = async (req, res) => {
    res.render('addCategory')
  }
  static updateCategoryView = async (req, res, next) => {
    try {
      const findCategory = await categoryModel.findOne({ _id: req.params.categoryId, isDeleted: false })
      res.render('categoryUpdate', { findCategory: findCategory })
    } catch (error) {
      next(error)
    }

  }
  /**
   * This function used to get all data from database collection/table
   * @param {mongoId} req.params.categoryId categoryID
   * @param {integer} req.query.page page - page Number
   * @param {integer} req.query.perPage perPage - per page data
   * @param {string} req.query.fieldName fieldName ex. name
   * @param {string} req.query.sortOrder sortOrder ex. "asc", "desc"
   * @param {string} req.query.search search 
   * @returns {array} list of categories
   */
  static categoryList = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const pagination = services.pagination(req.query.page, req.query.perPage, req.query.sortField, req.query.sortType, "name")
      let filter = { isDeleted: false };
      var search = req.query.search;
      if (search) {
        filter.$or = [{ name: { $regex: `${search}`, $options: "i" } }]
      };
      const categoryData = await categoryModel
        .aggregate([
          { $match: filter },
          {
            $project: {
              _id: 1,
              name: 1,
            },
          },
          { $sort: pagination.sortObj }
        ])
        .collation({ locale: "en" })
        .skip(pagination.skipRecord)
        .limit(pagination.perPage)

      const totalRecords = await categoryModel.find({ isDeleted: false }).countDocuments()
      const categoryDataList = services.dataList(categoryData, totalRecords, pagination.page, pagination.perPage)
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.SUCCESSFUL, categoryDataList);
    } catch (error) {
      next(error)
    }
  };
  /**
 * This function used to get single data from database collection/table by category Id
 * @param {mongoId} req.params.categoryId categoryId 
 * @returns {object} object of collection/table
 */
  static editCategory = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const categoryData = await categoryModel.findOne({
        isDeleted: false,
        _id: req.params.categoryId,
      });

      if (!categoryData) {
        return send(
          res,
          httpCode.BAD_REQUEST,
          httpCode.BAD_REQUEST,
          message.CATEGORY_NOT_FOUND,
          null
        );
      }

      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.SUCCESSFUL, {
        _id: categoryData._id,
        name: categoryData.name,
      });
    } catch (error) {
      next(error)
    }
  };
  /**
   * This function add new data in database/table
   * @param {string} req.body.name name
   * @returns {object} show Id of object which will create
   */
  static addCategory = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const checkCategory = await categoryModel.findOne({
        name: req.body.name,
        isDeleted: false,
      });
      if (checkCategory) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.CATEGORY_EXITS, null);
      }

      const addCategory = new categoryModel({
        name: req.body.name,
      });
      const categoryData = await addCategory.save();

      if (!categoryData) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.CATEGORY_ADDED_SUCCESSFULLY, { _id: categoryData._id, });
    } catch (error) {
      next(error)
    }
  };
  /**
   * This function update or edit existing data
   * @param {mongoId} req.params.categoryId categoryId
   * @param {string} req.body.name name
   * @param {object} res object with successCode, message and ID
   * @returns {object} category Id
   */
  static updateCategory = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const categoryData = await categoryModel.findOne({
        _id: req.params.categoryId,
        isDeleted: false,
      });

      if (!categoryData) {
        return send(res, httpCode.NOTFOUND, httpCode.NOTFOUND, message.CATEGORY_NOT_FOUND, null);
      }
      const checkCategory = await categoryModel.findOne({
        _id: { $ne: req.params.categoryId },
        name: req.body.name,
        isDeleted: false,
      });

      if (checkCategory) {
        return send(
          res,
          httpCode.BAD_REQUEST,
          httpCode.BAD_REQUEST,
          message.CATEGORY_EXITS,
          null
        );
      }
      categoryData.name = req.body.name;
      const categoryUpdate = await categoryData.save();
      if (!categoryUpdate) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.CATEGORY_UPDATED_SUCCESSFULLY, { _id: categoryUpdate._id, })
    } catch (error) {
      next(error)
    }
  };

  /**
   * This function delete data/category from database
   * @param {mongoId} req.params.categoryId categoryId
   * @param {object} res send object with successCode and delete message
   * @returns {object} delete category message
   */
  static deleteCategory = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const findCategory = await categoryModel.findOne({
        _id: req.params.categoryId,
        isDeleted: false,
      });
      if (!findCategory) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.CATEGORY_NOT_FOUND, null
        )
      }
      findCategory.isDeleted = true;
      await findCategory.save();
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.CATEGORY_DELETED_SUCCESSFULLY, null
      )
    } catch (error) {
      next(error)
    }
  };
}

export default categoryController;
