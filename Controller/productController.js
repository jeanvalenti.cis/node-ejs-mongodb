import message from "../helper/messages.js";
import httpCode from "../helper/httpCodes.js";
import services from "../helper/index.js";
import productModel from "../models/productModel.js";
import productImageModel from "../models/imageModel.js";
import categoryModel from "../models/categoryModel.js";
import fs from "fs";
import { ObjectId } from "mongodb";
const send = services.setResponse;

class productController {
  /**
   * Render ejs product listing page 
   */
  static listAndDeleteProduct = async (req, res) => {
    res.render('productListAndDelete')
  }

  /**
   * It will render ejs/html page to add product
   * @param {object} res render ejs page to add product
   */
  static addProductView = async (req, res) => {
    const listCategory = await categoryModel.find({ isDeleted: false })
    res.render('productAdd', { listCategory: listCategory })
  }

  /**
   * This function render ejs/html page
   * @param {objectId} req.param.productId productId
   * @param {array} res all products from database collection/table
   */
  static updateProductView = async (req, res, next) => {
    try {
      const listProduct = await productModel.aggregate([{
        $match: {
          $and: [
            { _id: ObjectId(req.params.productId) },
            { isDeleted: false },
          ],
        },
      }, {
        $lookup: {
          from: "categories",
          localField: "categoryId",
          foreignField: "_id",
          as: "category",
        },
      },
      {
        $lookup: {
          from: "images",
          localField: "_id",
          foreignField: "productId",
          as: "images",
        },
      },
      {
        $project: {
          _id: 1,
          name: 1,
          categoryId: 1,
          category: "$category.name",
          images: 1,
          description: 1,
          price: 1,
          sellPrice: 1,
        },
      },])
      res.render('productUpdate', { listProduct: listProduct[0] })
    } catch (error) {
      next(error)
    }
  }

  /**
   * This function use to add products in database/table
   * @param {string} req.body.name name of product
   * @param {ObjectId} req.body.categoryId Id of category
   * @param {string} req.body.description product description  
   * @param {number} req.body.price product price
   * @param {number} req.body.sellPrice product sell price
   * @param {object} res responseCode ,responseMessage and object Id 
   * @returns {promise} promise to save data in dataBase 
   */
  static addProduct = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const checkProduct = await productModel.findOne({
        name: req.body.name,
        isDeleted: false,
      });

      if (checkProduct) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.PRODUCT_EXITS, null);
      }

      const matchCategory = await categoryModel.findOne({ isDeleted: false, _id: req.body.categoryId });
      if (!matchCategory) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.CATEGORY_NOT_FOUND, null);
      }

      const addProduct = new productModel({
        categoryId: req.body.categoryId,
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        sellPrice: req.body.sellPrice,
      });

      if (req.files) {
        const imageData = services.uploadImage(req.files.images, addProduct._id);
        if (imageData.length == 0) {
          return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.FILE_NOT_FOUND, null);
        }

        const insertImages = await productImageModel.insertMany(imageData);
        if (!insertImages) {
          return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
        }
      }

      const productData = await addProduct.save();
      if (!productData) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }

      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.ADDED_SUCCESSFULLY, { _id: productData._id });
    } catch (error) {
      next(error);
    }
  };

  /**
   * This function use to list all products from database/table
   * @param {number} req.query.page page number
   * @param {number} req.query.perPage perPage data
   * @param {string} req.query.fieldName fieldName ex. name
   * @param {string}} req.query.sortOrder sortOrder ex. "asc", "desc"
   * @param {string} req.query.search search
   * @param {object} res responseCode, responseMessage, list of all data
   * @returns {promise} to list all products data
   */
  static productList = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const pagination = services.pagination(req.query.page, req.query.perPage, req.query.sortField, req.query.sortType)
      let filter = { isDeleted: false };
      if (req.query.search) {
        filter.$or = [
          { name: { $regex: req.query.search, $options: "i" } },
          { description: { $regex: req.query.search, $options: "i" } },
        ]
      };

      const productData = await productModel
        .aggregate([
          { $match: filter },
          {
            $lookup: {
              from: "categories",
              localField: "categoryId",
              foreignField: "_id",
              as: "category",
            },
          },
          {
            $lookup: {
              from: "images",
              localField: "_id",
              foreignField: "productId",
              as: "images",
            },
          },
          {
            $project: {
              _id: 1,
              name: 1,
              categoryId: 1,
              category: { $arrayElemAt: ["$category.name", 0] },
              images: {
                "$map": {
                  "input": "$images",
                  "as": "image",
                  "in": { "$concat": [process.env.BASE_URL, "$$image.imagePath"] }
                }
              },
              description: 1,
              price: 1,
              sellPrice: 1,
            },
          },
          { $sort: pagination.sortObj }
        ])
        .collation({ locale: "en" })
        .skip(pagination.skipRecord)
        .limit(pagination.perPage)

      if (!productData) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }
      const totalRecords = await productModel.find({ isDeleted: false }).countDocuments()
      const productDataList = services.dataList(productData, totalRecords, pagination.page, pagination.perPage)
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.SUCCESSFUL, productDataList)
    } catch (error) {
      next(error);
    }
  };

  /**
   * This function use to get single product data from dataBase/table
   * @param {objectId} req.params.productId productId  
   * @param {object} res responseCoed, responseMessage, responseData
   * @returns {promise} promise of get data
   */
  static editProduct = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const productData = await productModel.aggregate([
        {
          $match: {
            $and: [
              { _id: ObjectId(req.params.productId) },
              { isDeleted: false },
            ],
          },
        },
        {
          $lookup: {
            from: "categories",
            localField: "categoryId",
            foreignField: "_id",
            as: "category",
          },
        },
        {
          $lookup: {
            from: "images",
            localField: "_id",
            foreignField: "productId",
            as: "image",
          },
        },
        {
          $project: {
            _id: 1,
            categoryId: 1,
            category: { $arrayElemAt: ["$category.name", 0] },
            images: [{ $arrayElemAt: ["$image.imagePath", 0] }, { $arrayElemAt: ["$image.imagePath", 1] }],
            name: 1,
            description: 1,
            price: 1,
            sellPrice: 1,
          },
        } 
      ]);
      if (!productData || productData[0] == undefined) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.SUCCESSFUL, productData[0]);
    } catch (error) {
      next(error);
    }
  };

  /**
   * This function use to edit/update data from database/table
   * @param {objectId} req.params.productId productId
   * @param {string} req.body.name name of product
   * @param {string} req.body.description product description
   * @param {number} req.body.price price of product
   * @param {number} req.body.sellPrice sell price of product 
   * @param {object} res responseCode, responseMessage, responseData: productId
   * @returns {promise} promise to update data
   */
  static updateProduct = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }

      const productData = await productModel.findOne({ _id: req.params.productId, isDeleted: false });
      if (!productData) {
        return send(res, httpCode.NOTFOUND, httpCode.NOTFOUND, message.SOMETHING_WENT_WRONG, null);
      }

      const checkProduct = await productModel.findOne({
        _id: { $ne: req.params.productId },
        name: req.body.name,
        isDeleted: false,
      });

      if (checkProduct) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.PRODUCT_EXITS, null);
      }

      productData.name = req.body.name;
      productData.description = req.body.description;
      productData.price = req.body.price;
      productData.sellPrice = req.body.sellPrice;

      if (req.files) {
        const imageData = services.uploadImage(req.files.images, req.params.productId);
        const productImages = await productImageModel.insertMany(imageData);
        if (!productImages) {
          return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
        }
      }
      const updateProduct = await productData.save();
      if (!updateProduct) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.UPDATED_SUCCESSFULLY, { _id: productData._id, });
    } catch (error) {
      next(error);
    }
  };

  /**
   * This function use to delete product softly
   * @param {objectId} req.params.productId productId
   * @param {object} res responseCode, responseMessage, ObjectId of product
   * @returns {promise} promise of delete product
   */
  static deleteProduct = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const findProduct = await productModel.findOne({
        _id: req.params.productId,
        isDeleted: false,
      });

      if (!findProduct) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.ID_NOT_FOUND, null);
      }

      findProduct.isDeleted = true;
      await findProduct.save();
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.PRODUCT_DELETED_SUCCESSFULLY, null);
    } catch (error) {
      next(error);
    }
  };

  /**
   * This function use to delete images permanently from database/table and file/server 
   * @param {ObjectId} req.params.imageId imageId
   * @param {object} res responseCode, response
   * @returns {promise} http code, delete message
   */
  static deleteFiles = async (req, res, next) => {
    //Deleting old images
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const deleteFile = await productImageModel.findByIdAndDelete({
        _id: req.params.imageId,
        isDeleted: false,
      });

      if (!deleteFile) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.ID_NOT_FOUND, null);
      }
      //deleting files
      if (fs.existsSync(`.${deleteFile.imagePath}`)) {
        fs.unlink(`.${deleteFile.imagePath}`, (error) => {
          if (error) {
            return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
          }
        });
      }
      //Deleting directory if empty
      fs.readdir(`./uploads/${deleteFile.productId}`, (error, file) => {
        if (error) {
          return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
        }
        if (file.length == 0) {
          fs.rmdir(`./uploads/${deleteFile.productId}`, (error) => {
            if (error) {
              return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
            }
          });
        }
      });
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.IMAGE_DELETED_SUCCESSFULLY, null);
    } catch (error) {
      next(error);
    }
  };
}

export default productController;
