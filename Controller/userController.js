import userModel from "../models/userSchema.js";
import message from "../helper/messages.js";
import httpCode from "../helper/httpCodes.js";
import services from "../helper/index.js";
import ejs from "ejs";
import { ObjectId } from "mongodb";
const send = services.setResponse;

class userController {
  // This function render login page
  static loginView = async (req, res) => {
    res.render('login')
  }
  // This function render page to add new user
  static addUserView = async (req, res) => {
    res.render('addUser')
  }
  // This function render home page
  static homePageView = async (req, res) => {
    res.render('homePage')
  }
  // This function render user listing page
  static userListView = async (req, res) => {
    res.render('userList')
  }

  /**
   * This function render update user page
   * @param {ObjectId} req.params.userId userId
   * @param {object} res single user data
   * @param {*} next next method call if error occurred
   */
  static updateUserView = async (req, res, next) => {
    try {
      const getUser = await userModel.findOne({ _id: req.params.userId, isDeleted: false })
      res.render('userUpdate', { getUser: getUser })
    } catch (error) {
      next(error)
    }
  }
  // This function render forgot password page
  static forgotPasswordView = async (req, res) => {
    res.render('forgotPasswordView')
  }
  // This function render new password page
  static setNewPasswordView = async (req, res) => {
    res.render('serNewPassword', { token: req.params.token })
  }

  /**
   * This function use to list all user Data from database/table
   * @param {number} req.query.page page Number
   * @param {number} req.query.perPage perPage data
   * @param {string} req.query.sortField sort field name ex. firstName
   * @param {string} req.query.sortType sort type ex. "asc" , "desc"
   * @param {string} req.query.search search  
   * @param {array} res responseCode, responseMessage, list of all user 
   * @param {error} next next method call if error occurred
   * @returns {promise} promise to list all users data
   */
  static userList = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      } 
      const pagination = services.pagination(req.query.page, req.query.perPage, req.query.sortField, req.query.sortType)
      // Searching...
      let filter = { isDeleted: false };
      var search = req.query.search;
      if (search) {
        filter.$or = [
          { firstName: { $regex: search, $options: "i" } },
          { lastName: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
          { password: { $regex: search, $options: "i" } },
          { mobileNo: { $regex: search, $options: "i" } },
        ]
      }

      const userData = await userModel
        .aggregate([
          { $match: filter },
          {
            $project: {
              _id: 1,
              firstName: 1,
              lastName: 1,
              email: 1,
              password: 1,
              mobileNo: 1,
            },
          },
          { $sort: pagination.sortObj }
        ])
        .collation({ locale: "en" })
        .skip(pagination.skipRecord)
        .limit(pagination.perPage);
      if (!userData) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG);
      }

      const totalRecords = await userModel.find({ isDeleted: false }).countDocuments()
      const userDataList = services.dataList(userData, totalRecords, pagination.page, pagination.perPage)
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.SUCCESSFUL, userDataList,);
    } catch (error) {
      next(error);
    }
  };

  /**
   * This function use to add new user in database/table
   * @param {string} req.body.email user email
   * @param {string} req.body.firstName user first name
   * @param {string} req.body.LastName user last name
   * @param {string} req.body.password user password
   * @param {string} req.body.mobileNo user mobile number
   * @param {object} next next method will call if error occurred
   * @returns {promise} promise to add user in database/table
   */
  static registration = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }

      const checkUser = await userModel.findOne({ email: req.body.email })
      if (checkUser) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.USER_ALREADY_EXITS, null);
      }
      const addUser = new userModel({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        mobileNo: req.body.mobileNo,
      });
      const userData = await addUser.save();
      if (!userData) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.REGISTRATION_SUCCESSFUL, { _id: userData._id, });
    } catch (error) {
      next(error);
    }
  };

  /**
   * This function use to logIn/access  with token
   * @param {string} req.body.email email 
   * @param {string} req.body.password user saved password 
   * @param {object} res responseCode, responseMessage, userName, Id , token
   * @returns {promise} promise verify or login user 
   */
  static logIn = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }

      const checkUser = await userModel.findOne({
        email: req.body.email,
      });
      if (!checkUser) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.INVALID_EMAIL, null);
      }

      if (checkUser.email == req.body.email && checkUser.password == req.body.password) {
        const newToken = services.generateToken(res, checkUser._id, process.env.EXPIRE_TOKEN);
        const userData = {
          id: checkUser._id,
          firstName: checkUser.firstName,
          Token: newToken,
        };
        return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.LOGIN_SUCCESSFUL, userData);
      }
      return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.INVALID_PASSWORD, null);
    } catch (error) {
      next(error);
    }
  };
  /**
   * This function use to get single data from dataBase/table by unique id
   * @param {ObjectId} req.param.userId  user Id
   * @param {object} res responseCode, responseMessage, userDetails
   * @param {object} next next method call if error occurred
   * @returns {promise} return promise to list user's data
   */
  static editUser = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const userData = await userModel.findOne({
        isDeleted: false,
        _id: req.params.userId,
      });

      if (!userData) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.USER_NOT_FOUND, null);
      }
      const userDetails = {
        _id: userData._id,
        firstName: userData.firstName,
        lastName: userData.lastName,
        email: userData.email,
        password: userData.password,
        mobileNo: userData.mobileNo,
      }
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.SUCCESSFUL, userDetails);
    } catch (error) {
      next(error)
    }
  };

  /**
 * This function use to update/edit user details in database/table
 * @param {ObjectId} req.params.userId user unique Id
 * @param {string} req.body.name user name
 * @param {object} res responseCode, responseMessage, userId
 * @returns {promise} promise to update/edit user data
 */
  static updateUser = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      if (!req.params.userId) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null)
      }
      const userData = await userModel.findOne({
        _id: req.params.userId,
        isDeleted: false,
      });

      if (!userData) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.USER_NOT_FOUND, null);
      }
      const checkUser = await userModel.findOne({
        _id: { $ne: req.params.userId },
        email: req.body.email,
        isDeleted: false,
      });

      if (checkUser) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.USER_EXITS, null);
      }

      userData.firstName = req.body.firstName,
        userData.lastName = req.body.lastName,
        userData.email = req.body.email,
        userData.password = req.body.password,
        userData.mobileNo = req.body.mobileNo

      const userUpdate = await userData.save();
      if (!userUpdate) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }
      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.USER_UPDATED_SUCCESSFULLY, { _id: userUpdate._id, })
    } catch (error) {
      next(error)
    }
  };

  /**
   * This function use to delete user softly from database/table
   * @param {*} req.params.UserId user uniq Id
   * @param {*} res responseCode, responseMessage
   * @returns {promise} promise to delete user
   */
  static deleteUser = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }
      const findUser = await userModel.findOne({
        _id: req.params.userId,
        isDeleted: false,
      });
      if (!findUser) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.USER_NOT_FOUND, null)
      }
      findUser.isDeleted = true;

      await findUser.save();
      const userId = { userId: findUser._id }

      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.USER_DELETED_SUCCESSFULLY, null)
    } catch (error) {
      next(error)
    }
  };

  /**
   * This function use to reset password by link which will send on user email and mobile number 
   * @param {string} req.body.email user email 
   * @param {*} res responseCode, responseMessage
   * @param {*} next call if error occurred
   * @returns {promise} promise to send reset password  link on user email and mobile number
   */
  static forgotPassword = async (req, res, next) => {
    try {
      if (services.validateResult(req, res)) {
        return;
      }

      const checkUser = await userModel.findOne({
        email: req.body.email,
        isDeleted: false
      });

      if (!checkUser) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.INVALID_EMAIL, null);
      }

      const token = services.generateToken(res, checkUser._id, "10m");
      const sendEmail = services.emailVerification(res, checkUser.email, token)
      const sendMessage = services.numberVerification(checkUser.mobileNo, token)

    } catch (error) {
      next(error);
    }
  }

  /**
   * This function for change password
   * @param {token} req token
   * @param {object} res responseCode, responseMessage
   * @param {error} next call if error occurred  
   * @returns {promise} promise to change password
   */
  static setNewPassword = async (req, res, next) => {
    try {
      const decodeToken = services.jwtVerification(req.params.token);
      if (decodeToken == true) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.INVALID_TOKEN, null)
      }

      const userData = await userModel.findOne({ _id: decodeToken[1], isDeleted: false })
      userData.password = req.body.password
      const userUpdate = await userData.save();
      if (!userUpdate) {
        return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null);
      }

      return send(res, httpCode.SUCCESS, httpCode.SUCCESS, message.PASSWORD_CHANGED_SUCCESSFULLY, null)
    } catch (error) {
      next(error)
    }
  }
}
export default userController;
