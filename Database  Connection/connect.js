import mongoose from "mongoose";

/**
 * Connecting with dataBase
 */
const ConnectToDataBase = async () => {
  try {
    await mongoose.connect(process.env.DATABASE_URL);
    console.log("Connected Successfully");
  } catch (error) {
    console.log("Connection failed")
  }
};

export default ConnectToDataBase;
