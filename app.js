import express from "express";
import ConnectToDataBase from "./Database  Connection/connect.js";
import routes from "./routes/userRoute.js";
import categoryRoute from "./routes/categoryRoute.js";
import productRoute from "./routes/productRoute.js";
import dotenv from "dotenv";
import swaggerUI from "swagger-ui-express";
import swaggerJsDoc from "swagger-jsdoc";
import bodyParser from "body-parser";
import fileUpload from "express-fileupload";
import errorHandler from "./error/handler.js";
import { join } from "path";

dotenv.config();
const app = express();
const port = process.env.PORT;
ConnectToDataBase();
app.set("view engine", "ejs")

//setUp swagger UI
const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Registration API",
      version: "1.0.0",
      description: "A simple Express Library API",
    },
    components: {
      securitySchemes: {
        bearerAuth: {
          type: "http",
          scheme: "bearer",
          bearerFormat: "JWT",
        },
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  },
  apis: ["./swaggerDocument/*.js"],
};

app.use("/api", swaggerUI.serve, swaggerUI.setup(swaggerJsDoc(options)));
app.use("/uploads", express.static("./uploads"));
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(fileUpload());
app.use(express.static('views'));
app.use("/admin/user", routes);
app.use("/admin/category", categoryRoute);
app.use("/admin/product", productRoute);
app.use(errorHandler);
app.use((req, res) => { res.status(404).render("error404") })
app.listen(port, () => { console.log(process.env.BASE_URL); });
