import httpCode from "../helper/httpCodes.js";
import services from "../helper/index.js";
import message from "../helper/messages.js";
const send = services.setResponse

/**
 * Common function for error handling
 * @param {*} error if error occurred
 * @param {*} req.path
 * @param {*} res send response
 * @param {*} next 
 */
function errorHandler(error, req, res, next) {
    console.log('error :>> ', error);
    res.render('errorPage')
    let detail = {}
    let log = {}
    detail.message = error.message
    detail.stack = error.stack
    log.title = error.name
    log.routeName = req.path 
    log.path = error.lineNumber
    log.errorData = detail
    // return send(res, httpCode.SERVER_ERROR, httpCode.SERVER_ERROR, message.SOMETHING_WENT_WRONG, log)

}

export default errorHandler