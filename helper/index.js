import { validationResult } from "express-validator";
import jwt from "jsonwebtoken";
import moment from "moment";
import httpCode from "./httpCodes.js";
import message from "./messages.js";
import fs from "fs";
import nodemailer from "nodemailer";
import Twilio from "twilio"
import ejs from "ejs";


/**
 * Send response with common function
 * @param {*} HttpCode http status code
 * @param {*} message response message
 * @param {*} data data from user
 * @returns http code, message and data
 */
const response = (HttpCode, message, data) => {
  if (data == null && " ") {
    return {
      responseCode: HttpCode,
      responseMessage: message,
    };
  }
  return {
    responseCode: HttpCode,
    responseMessage: message,
    responseData: data,
  };
};

class services {
  /**
   * Validation of input value
   * @param {*} req check validation of input
   * @param {*} res
   * @returns httpCode, message, validation messages
   */
  static validateResult(req, res) {
    const { errors } = validationResult(req);
    if (errors.length > 0) {
      res.status(httpCode.BAD_REQUEST).send(response(httpCode.BAD_REQUEST, message.VALIDATION_FAILED, errors));
      return true;
    }
    return false;
  }

  /**
   * This function use to generateToken
   * @param {*} res 
   * @param {*} id user id 
   * @returns Access token
   */
  static generateToken(res, userId, expireTime) {
    if (userId == null && " ") {
      return res.status(httpCode.BAD_REQUEST).send(response(httpCode.BAD_REQUEST, message.INVALID_ID));
    }

    const token = jwt.sign({ _id: `${userId}` }, process.env.SECURITY_KEY, {
      expiresIn: expireTime,
    });
    return token;
  }

  /**
   * This function use to verify token and set expire time
   * @param {*} token
   * @returns true or false
   */
  static jwtVerification(token) {
    try {
      const verifyToken = jwt.verify(token, process.env.SECURITY_KEY);
      const expiryToken = moment().unix() > verifyToken.exp;
      if (expiryToken) {
        return true;
      }
      return [false, verifyToken._id]
    } catch (error) {
      return true;
    }
  }

  /**
   * This function use to uploads image in file and database
   * @param {*} files uploads by user
   * @param {*} id product Id
   * @returns Image path and product Id
   */
  static uploadImage(files, id) {
    if (files.length == undefined) {
      files = [files];
    }

    if (!fs.existsSync(`./uploads/${id}/`)) {
      fs.mkdirSync(`./uploads/${id}/`, { recursive: true });
    }
    let randomNumber = 100000 + Math.floor(Math.random() * 900000) + moment().unix()
    let imageData = [];
    files.forEach((image) => {
      image.mv(`./uploads/${id}/` + randomNumber + image.name, (error) => {
        if (error) {
          console.log("error :>> ", error);
        }
      });
      imageData.push({
        productId: id,
        imagePath: `/uploads/${id}/` + randomNumber + image.name,
      });
    });
    return imageData;
  }

  /**
   * Common function for send response
   * @param {*} res 
   * @param {*} httpStatus http status code
   * @param {*} httpCode http status code
   * @param {*} message message of success/other
   * @param {*} data send by user
   * @returns status, httpCoe , message, data
   */
  static async setResponse(res, httpStatus, httpCode, message, data) {
    await res.send(response(httpCode, message, data));
  }

  static async emailVerification(res, userEmail, token) {
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'jaivikvaghasiya22@gmail.com',
        pass: 'kuxthcmwpvbwkxqf'
      }
    });

    const data = await ejs.renderFile(process.cwd() + "/views/emailLayout.ejs", { token: token });
    var mailOptions = {
      from: 'jaivikvaghasiya22@gmail.com',
      to: userEmail,
      subject: 'Reset your IndiaMart password',
      html: data
    }

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return this.setResponse(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.SOMETHING_WENT_WRONG, null)
      }
    });
    return this.setResponse(res, httpCode.SUCCESS, httpCode.SUCCESS, message.EMAIL_SENT_SUCCESSFULLY, null)
  }

  static async numberVerification(userMobileNumber, token) {
    const client = new Twilio(process.env.ACCOUNT_SID, process.env.AUTH_TOKEN);
    console.log("client", client)
    client.messages.create({
      body: 'Click on url to reset your password:  ' + process.env.BASE_URL + '/admin/user/new/password/view/' + token,
      from: '+18149147966',
      to: userMobileNumber
    }).then(message => console.log("message:", message)).catch(error => console.log(error))
  }

  static pagination = (pageNumber, perPageValue, sortField, sortType, defaultSortField) => {
    const page = pageNumber > 0 ? parseInt(pageNumber) : 1;
    const perPage = perPageValue > 0 ? parseInt(perPageValue) : 10;
    let sortObj = {
      defaultSortField:1
    };

    if (sortField != undefined && sortType == "asc") {
      sortObj = {
        [sortField]: 1,
      };
    }
    if (sortField != undefined && sortType == "desc") {
      sortObj = {
        [sortField]: -1,
      };
    }
    return { page, skipRecord: (page - 1) * perPage, perPage, sortObj }
  }

  static dataList = (data, totalRecord, pageNumber, perPagRecord) => {
    const dataList = {}
    dataList.list = data
    dataList.pageNumber = pageNumber
    dataList.perPageRecords = perPagRecord
    dataList.RecordsFiltered = data.length
    dataList.totalRecords = totalRecord
    return dataList
  }
}
export default services;
