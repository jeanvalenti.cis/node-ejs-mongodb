import services from "../helper/index.js";
import message from "../helper/messages.js";
import httpCode from "../helper/httpCodes.js";
import userModel from "../models/userSchema.js";
const send = services.setResponse;

/**
 * This function use to authenticate user
 * @param {*} req 
 * @param {*} res 
 * @param {*} next call error if occurred
 * @returns verify token
 */
const  authenticateUser = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      return send(res,httpCode.UNAUTHORIZE,httpCode.UNAUTHORIZE,message.UNAUTHORIZE_TOKEN,null);
    }
    const token = req.headers.authorization.split(" ")[1];
    const decodeToken = services.jwtVerification(token); 
    if (decodeToken == true) {
      return send(res, httpCode.BAD_REQUEST, httpCode.BAD_REQUEST, message.INVALID_TOKEN, null)
    }

    let user  = await userModel.findOne({_id:  decodeToken[1] })
    req.user = user
    next();
  } catch (error) {
    next(error)
  }
};

export default authenticateUser;
