import mongoose from "mongoose";
var Schema = mongoose.Schema;
/**
 * Defining  image Schema 
 */
const imageSchema = new mongoose.Schema(
  {
    productId: {
      type: Schema.Types.ObjectId,
      ref: 'products',
      required: true,
    },
    imagePath: {
      type: String,
      required: true,
      trim: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  { timestamps: true }
);
const productImageModel = mongoose.model("images", imageSchema);

export default productImageModel;
