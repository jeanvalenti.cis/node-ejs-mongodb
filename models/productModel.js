import mongoose from "mongoose";
var Schema = mongoose.Schema;

//Defining Schema
const productSchema = new Schema(
  {
    categoryId: {
      type: Schema.Types.ObjectId,
      ref: 'categories',
      require: true,
    },
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
    },
    price: {
      type: Number,
      require: true,
    },
    sellPrice: {
      type: Number,
      require: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },

    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
);

const productModel = mongoose.model("Products", productSchema);

export default productModel;
