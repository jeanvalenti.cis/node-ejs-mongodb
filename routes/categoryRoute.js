import categoryController from "../Controller/categoryController.js";
import express from "express";
import { query, body, param } from "express-validator";
import message from "../helper/messages.js";
import authenticateUser from "../middleware/userMiddleware.js";

const router = express.Router();
/**
 * get category route
 */
router.get("/list", authenticateUser,
  [
    query("sortField").isString().withMessage(message.INVALID_SORT_FIELD).optional(),
    query("sortType").  isIn(["asc", "desc"]).withMessage(message.MUST_BE_ASC_OR_DESC).optional(),
  ],
  categoryController.categoryList
);

//getById category route
router.get(
  "/edit/:categoryId",
  authenticateUser,
  [
    param("categoryId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
  ],
  categoryController.editCategory
);

/**
 * post category route
 */
router.post(
  "/add", authenticateUser,
  [body("name").notEmpty().withMessage(message.NAME_IS_REQUIRED)],
  categoryController.addCategory
);

/**
 * put category route
 */
router.put(
  "/update/:categoryId", authenticateUser,
  [
    param("categoryId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
    body("name").notEmpty().withMessage(message.NAME_IS_REQUIRED),
  ],
  categoryController.updateCategory
);

/**
 * delete category route
 */
router.delete(
  "/delete/:categoryId", authenticateUser,
  [
    param("categoryId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
  ],
  categoryController.deleteCategory
);

router.get('/list/view', categoryController.categoryListAndDeleteView)
router.get('/add/view', categoryController.addCategoryView
)
router.get('/update/view/:categoryId',
  [
    param("categoryId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
  ],
  categoryController.updateCategoryView
)

export default router;
