import productController from "../Controller/productController.js";
import express from "express";
import {query, body, param } from "express-validator";
import message from "../helper/messages.js";
import authenticateUser from "../middleware/userMiddleware.js";

const router = express.Router();

/**
 * post route
 */
router.post(
  "/add", authenticateUser,
  [
    body("categoryId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
    body("name").notEmpty().withMessage(message.NAME_IS_REQUIRED),
    body("price")
      .isInt()
      .withMessage(message.INVALID_PRICE)
      .notEmpty()
      .withMessage(message.PRICE_REQUIRED),
    body("sellPrice")
      .isInt()
      .withMessage(message.INVALID_PRICE)
      .notEmpty()
      .withMessage(message.PRICE_REQUIRED),
  ],
  productController.addProduct
);

/**
 * `get product` route 
 */
let sortType  = ["asc", "desc"]
router.get("/list", authenticateUser,

[
  query("sortField").isString().withMessage(message.INVALID_SORT_FIELD).optional({checkFalsy: true}),
  query("sortType").isString().withMessage(message.INVALID_SORT_TYPE).isIn(["asc", "desc"]).withMessage(message.MUST_BE_ASC_OR_DESC).optional(),
],
  productController.productList
);


/**
 * get route by product Id
 */
router.get(
  "/edit/:productId", authenticateUser,
  [
    param("productId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID)
  ],
  productController.editProduct
);

/**
 * put/update product route
 */
router.put(
  "/update/:productId", authenticateUser,
  [
    param("productId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
    body("name").notEmpty().withMessage(message.NAME_IS_REQUIRED),
    body("price")
      .notEmpty()
      .withMessage(message.PRICE_REQUIRED)
      .isInt().toInt()
      .withMessage(message.INVALID_PRICE),
    body("sellPrice")
      .notEmpty()
      .withMessage(message.PRICE_REQUIRED)
      .isInt().toInt()
      .withMessage(message.INVALID_PRICE),
  ],
  productController.updateProduct
);

/**
 * delete product route
 */
router.delete(
  "/delete/:productId", authenticateUser,
  [
    param("productId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
  ],
  productController.deleteProduct
);

/**
 * delete product image route
 */
router.delete(
  "/deleteFiles/:imageId", authenticateUser,
  [
    param("imageId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
  ],
  productController.deleteFiles
);
router.get("/list/view", productController.listAndDeleteProduct)
router.get("/add/view", productController.addProductView)
router.get("/update/view/:productId", 
[
  param("productId")
    .notEmpty()
    .withMessage(message.ID_IS_REQUIRED)
    .isMongoId()
    .withMessage(message.INVALID_ID),
],productController.updateProductView)

export default router;
