import userController from "../Controller/userController.js";
import express from "express";
import { query, body, param } from "express-validator";
import message from "../helper/messages.js";
import authenticateUser from "../middleware/userMiddleware.js";

const router = express.Router();


// get users list route
router.get("/list", authenticateUser,
  [
    query("sortField").not().isString().withMessage(message.INVALID_SORT_FIELD),
    query("sortType").not().isString().withMessage(message.INVALID_SORT_TYPE).isIn(["asc", "desc"]).withMessage(message.MUST_BE_ASC_OR_DESC),
  ], userController.userList);

// Post/registration route
router.post(
  "/registration",
  [
    body("firstName").notEmpty().withMessage(message.FIRSTNAME_IS_REQUIRED),
    body("lastName").notEmpty().withMessage(message.LASTNAME_IS_REQUIRED),
    body("email")
      .isEmail()
      .withMessage(message.INVALID_EMAIL)
      .bail()
      .notEmpty()
      .withMessage(message.EMAIL_IS_REQUIRED),
    body("password")
      .notEmpty()
      .withMessage(message.PASSWORD_IS_REQUIRED)
      .bail()
      .isLength({ min: 5 })
      .withMessage(message.PASSWORD_HAS_MIN_8_CHARACTER),
    body("mobileNo")
      .notEmpty()
      .withMessage(message.MOBILE_NUMBER_REQUIRED)
      .bail()
      .isLength({ min: 10, max: 13 })
      .withMessage(message.INVALID_MOBILE_NUMBER)
  ],
  userController.registration
);

//  logIn user route
router.post(
  "/login",
  [
    body("email")
      .isEmail()
      .withMessage(message.INVALID_EMAIL)
      .bail()
      .notEmpty()
      .withMessage(message.EMAIL_IS_REQUIRED),
    body("password")
      .notEmpty()
      .withMessage(message.PASSWORD_IS_REQUIRED)
      .bail()
      .isLength({ min: 8 })
      .withMessage(message.PASSWORD_HAS_MIN_8_CHARACTER),
  ],
  userController.logIn
);

//get{id} user route
router.get(
  "/edit/:userId",
  [
    param("userId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .bail()
      .isMongoId()
      .withMessage(message.INVALID_ID),
  ],
  userController.editUser
);

//update user route
router.put(
  "/update/:userId",
  [
    param("userId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .bail()
      .isMongoId()
      .withMessage(message.INVALID_ID),
    body("firstName").notEmpty().withMessage(message.FIRSTNAME_IS_REQUIRED),
    body("lastName").notEmpty().withMessage(message.LASTNAME_IS_REQUIRED),
    body("email")
      .isEmail()
      .withMessage(message.INVALID_EMAIL)
      .bail()
      .notEmpty()
      .withMessage(message.EMAIL_IS_REQUIRED),
    body("password")
      .notEmpty()
      .withMessage(message.PASSWORD_IS_REQUIRED)
      .bail()
      .isLength({ min: 8 })
      .withMessage(message.PASSWORD_HAS_MIN_8_CHARACTER),
    body("mobileNo")
      .notEmpty()
      .withMessage(message.MOBILE_NUMBER_REQUIRED)
      .bail()
      .isLength({ min: 10, max: 13 })
      .withMessage(message.INVALID_MOBILE_NUMBER)
  ],
  userController.updateUser
);

// delete user route
router.delete(
  "/delete/:userId",
  [
    param("userId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .bail()
      .isMongoId()
      .withMessage(message.INVALID_ID),
  ],
  userController.deleteUser
);
router.post("/forgot/password",
  [
    body("email")
      .isEmail()
      .withMessage(message.INVALID_EMAIL)
      .bail()
      .notEmpty()
      .withMessage(message.EMAIL_IS_REQUIRED),
  ],
  userController.forgotPassword
);
router.put("/new/password/:token", userController.setNewPassword)

// ejs routes
router.get("/login/view", userController.loginView)
router.get("/registration/view", userController.addUserView)
router.get("/home/view", userController.homePageView)
router.get("/list/view", userController.userListView)
router.get("/update/view/:userId",
  [
    param("userId")
      .notEmpty()
      .withMessage(message.ID_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_ID),
  ], userController.updateUserView)
router.get("/forgot/password/view", userController.forgotPasswordView)
router.get("/new/password/view/:token",
  [
    param("token")
      .notEmpty()
      .withMessage(message.TOKEN_IS_REQUIRED)
      .isMongoId()
      .withMessage(message.INVALID_TOKEN),
  ], userController.setNewPasswordView)

export default router;
