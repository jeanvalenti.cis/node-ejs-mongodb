//__________________ Defining Schema_______________
/**
 * @swagger
 *  components:
 *      schemas:
 *          category:
 *              type: object
 *              properties:
 *                  _id:
 *                      type: string
 *                      example: 62d948bf35e9c145d8f776e3
 *                  name:
 *                      type: string
 *                      example: Tech
 *
 */



 // _________________Get Method________________
/**
 * @swagger
 * /admin/category/list:
 *  get:
 *      summary: This method get your data
 *      tags: [category]
 *      description: This method  apply for getting your data
 *      parameters:
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: perPage
 *          description: Return data of per page
 *          schema:
 *              type: integer
 *        - in: query
 *          name: fieldName
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *        - in: query
 *          name: search    
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          status:
 *                              type: integer
 *                              example: 200
 *                          messages:
 *                              type: string
 *                              example: Successful
 *                          data:
 *                              properties:
 *                                  list:
 *                                      type: array
 *                                      items:
 *                                          $ref: '#components/schemas/category'
 *
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 */

 // _________________Get Method________________
/**
 * @swagger
 * /admin/category/edit/{categoryId}:
 *  get:
 *      summary: This method get your data
 *      tags: [category]
 *      description: This method  apply for getting your data
 *      parameters:
 *        - in: path
 *          name: categoryId
 *          required: true
 *          description: find category by Id
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: get successful
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Registration successful
 *                              data:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62d948bf35e9c145d8f776e3
 *                                      name:
 *                                          type: string
 *                                          example: Tech
 *                                          
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 */

// __________________Post Method___________________

/**
 * @swagger
 * /admin/category/add:
 *  post:
 *      summary: Create new data
 *      tags: [category]
 *      description: add new category
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          name:
 *                              type: string
 *                              example: Tech
 *      responses:
 *          200:
 *              description: Post successful
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Category added successfully
 *                              data:
 *                                  type: object
 *                                  properties:
 *                                      _id: 
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:    
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________________Put method___________________
/**
 * @swagger
 * /admin/category/update/{categoryId}:
 *  put:
 *      summary: update Your data
 *      tags: [category]
 *      description: update page
 *      parameters:
 *        - in: path
 *          name: categoryId
 *          required: true
 *          description: Redirect to entered page number
 *          schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties: 
 *                          name:
 *                              type: string
 *                              example: fruits
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Category updated successfully
 *                              data:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________delete method__________
/**
 * @swagger
 * /admin/category/delete/{categoryId}:
 *  delete:
 *      summary: Delete data
 *      tags: [category]
 *      description: delete page
 *      parameters:
 *        - in: path
 *          name: categoryId
 *          required: true
 *          description: Delete your data
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Category deleted Successfully
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 *
 */