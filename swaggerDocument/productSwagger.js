   
//__________________ Defining Schema_______________

 /**
 * @swagger
 *  components:
 *      schemas:
 *          product:
 *              type: object
 *              properties:
 *                  _id:
 *                      type: string
 *                      example: 62d948bf35e9c145d8f776e3                      
 *                  categoryId:
 *                      type: string
 *                      example: 62d948bf35e9c145d8f776e3
 *                  name:
 *                      type: string
 *                      example: Tech
 *                  description:
 *                      type: string
 *                      example: About this product
 *                  price:
 *                      type: integer
 *                      example: 256
 *                  sellPrice:
 *                      type: integer
 *                      example: 299 
 *                  category:
 *                      type: string
 *                      example: Laptop
 */

// __________________Post Method___________________

/**
 * @swagger
 * /admin/product/add:
 *  post:
 *      summary: Create new data
 *      tags: [Product]
 *      description: add new category
 *      requestBody:
 *          required: true
 *          content:
 *              multipart/form-data:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          categoryId:
 *                              type: string
 *                          name:
 *                              type: string
 *                              example: Tech
 *                          description:
 *                              type: string
 *                          price:
 *                              type: number
 *                              example: 250
 *                          sellPrice:
 *                              type: number
 *                              example: 300
 *                          images:
 *                              type: array    
 *                              items:
 *                                  type: string
 *                                  format:  binary
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Successful
 *                              data:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62d948bf35e9c145d8f776e3
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 */

// _________________Get Method________________
/**
 * @swagger
 * /admin/product/list:
 *  get:
 *      summary: This method get your data
 *      tags: [Product]
 *      description: This method  apply for getting your data
 *      parameters:
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: perPage
 *          description: Return data of per page
 *          schema:
 *              type: integer
 *        - in: query
 *          name: fieldName
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *        - in: query
 *          name: search    
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          status:
 *                              type: integer
 *                              example: 200
 *                          messages:
 *                              type: string
 *                              example: Successful
 *                          data:
 *                              type: object
 *                              properties:
 *                                  list:
 *                                      type: array
 *                                      items:
 *                                          $ref: '#components/schemas/product'
 *
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 */


 // _________________Get Method________________
/**
 * @swagger
 * /admin/product/edit/{productId}:
 *  get:
 *      summary: This method get your data
 *      tags: [Product]
 *      description: This method  apply for getting your data
 *      parameters:
 *        - in: path
 *          name: productId
 *          required: true
 *          description: find product by Id
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: get successful
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Successful
 *                              data:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62d948bf35e9c145d8f776e3
 *                                      categoryId:
 *                                          type: string
 *                                          example: 62d948bf35e9c145d8f776e3
 *                                      name:
 *                                          type: string
 *                                          example: Tech
 *                                      description:
 *                                          type: string
 *                                          example: About this product
 *                                      price:
 *                                          type: integer
 *                                          example: 256
 *                                      sellPrice:
 *                                          type: integer
 *                                          example: 299
 *                                      category:
 *                                          type: string
 *                                          example: Laptop
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 */

// __________________Put method___________________
/**
 * @swagger
 * /admin/product/update/{productId}:
 *  put:
 *      summary: update Your data
 *      tags: [Product]
 *      description: update your product details
 *      parameters:
 *        - in: path
 *          name: productId
 *          required: true
 *          description: Redirect to entered page number
 *          schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              multipart/form-data:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          categoryId:
 *                              type: string
 *                              example: 62eb7190a09b277ffa5de3bb
 *                          name:
 *                              type: string
 *                              example: Tech
 *                          description:
 *                              type: string
 *                              example: About product
 *                          price:
 *                              type: number
 *                              example: 250
 *                          sellPrice:
 *                              type: number
 *                              example: 300
 *                          images:
 *                              type: array    
 *                              items:
 *                                  type: string
 *                                  format:  binary
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Product updated successfully
 *                              data:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 */

// __________delete method__________
/**
 * @swagger
 * /admin/product/delete/{productId}:
 *  delete:
 *      summary: Delete data
 *      tags: [Product]
 *      description: delete page
 *      parameters:
 *        - in: path
 *          name: productId
 *          required: true
 *          description: Delete your data
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Product deleted Successfully
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________delete files__________
/**
 * @swagger
 * /admin/product/deleteFiles/{imageId}:
 *  delete:
 *      summary: Delete data
 *      tags: [Product]
 *      description: delete page
 *      parameters:
 *        - in: path
 *          name: imageId
 *          required: true
 *          description: Delete your data
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 200
 *                              messages:
 *                                  type: string
 *                                  example: Image deleted Successfully
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: integer
 *                                  example: 500
 *                              messages:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
