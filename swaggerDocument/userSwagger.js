//__________________ Defining Schema_______________
/**
 * @swagger
 *  components:
 *      schemas:
 *          responseData:
 *              type: object
 *              properties:
 *                  _id:
 *                      type: string
 *                      example: 62d948bf35e9c145d8f776e3
 *                  firstName:
 *                      type: string
 *                      example: Jaivik
 *                  lastName:
 *                      type: string
 *                      example: Vaghasiya
 *                  email:
 *                      type: string
 *                      example: example@gmail.com
 *                  password:
 *                      type: string
 *                      example: Password@123
 *                  mobileNo:
 *                      type: string
 *                      example: 9856451212
 *
 */



 // _________________Get Method________________
/**
 * @swagger
 * /admin/user/list:
 *  get:
 *      summary: This method get your responseData
 *      tags: [user]
 *      description: This method  apply for getting your responseData
 *      parameters:
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: perPage
 *          description: Return responseData of per page
 *          schema:
 *              type: integer
 *        - in: query
 *          name: sortField
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *        - in: query
 *          name: search    
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          responseCode:
 *                              type: integer
 *                              example: 200
 *                          responseMessage:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              properties:
 *                                  list:
 *                                      type: array
 *                                      items:
 *                                          $ref: '#components/schemas/responseData'
 *
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

// __________________Post Method___________________

/**
 * @swagger
 * /admin/user/registration:
 *  post:
 *      summary: Create your account
 *      tags: [user]
 *      description: registration page
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      properties:
 *                          firstName:
 *                              type: string
 *                              example: Jaivik
 *                          lastName:
 *                              type: string
 *                              example: Vaghasiya
 *                          email:
 *                              type: string
 *                              example: example@gmail.com
 *                          password:
 *                              type: string
 *                              example: Password@123
 *                          mobileNo:
 *                              type: string
 *                              example: 9856451212
 *      responses:
 *          200:
 *              description: When responseData has added Without an error..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: Registration successful
 *                              responseData:
 *                                  $ref: '#components/schemas/responseData'
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
// __________________Post Method___________________

/**
 * @swagger
 * /admin/user/login:
 *  post:
 *      summary: login to your account
 *      tags: [user]
 *      description: login page
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      properties:
 *                          email:
 *                              type: string
 *                              example: example@gmail.com
 *                          password:
 *                              type: string
 *                              example: Password@123
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: You are logged in successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *                                      firstName:
 *                                          type: string
 *                                          example: Name
 *                                      token:
 *                                          type: string
 *                                          example: eyJhbGciOiJIUzI1NiJ9.NjJlMjU5Mzg5NGUwOWNkM2VjZWI0M2Nj.bGDeCKTc3kmIs6VddW-dQNLypJxQyGgqKwtJ5U5BOcw
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// _________________Get Method________________
/**
 * @swagger
 * /admin/user/edit/{userId}:
 *  get:
 *      summary: This method get your responseData
 *      tags: [user]
 *      description: This method  apply for getting your responseData
 *      parameters:
 *        - in: path
 *          name: userId
 *          required: true
 *          description: find user by Id
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: get user successful
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: successful
 *                              responseData:
 *                                                                           
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

// __________________Put method___________________
/**
 * @swagger
 * /admin/user/update/{userId}:
 *  put:
 *      summary: update Your responseData
 *      tags: [user]
 *      description: update page
 *      parameters:
 *        - in: path
 *          name: userId
 *          required: true
 *          description: Redirect to entered page number
 *          schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties: 
 *                          firstName:
 *                              type: string
 *                              example: Jaivik
 *                          lastName:
 *                              type: string
 *                              example: Vaghasiya
 *                          email:
 *                              type: string
 *                              example: example@gmail.com
 *                          password:
 *                              type: string
 *                              example: Password@123
 *                          mobileNo:
 *                              type: string
 *                              example: 9856451212
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: user updated successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________delete method__________
/**
 * @swagger
 * /admin/user/delete/{userId}:
 *  delete:
 *      summary: Delete responseData
 *      tags: [user]
 *      description: delete page
 *      parameters:
 *        - in: path
 *          name: userId
 *          required: true
 *          description: Delete your Data
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: user deleted Successfully
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

/**
 * @swagger
 * /admin/user/forgot/password:
 *  post:
 *      summary: forgot your password
 *      tags: [user]
 *      description: reset your pas sword
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      properties:
 *                          email:
 *                              type: string
 *                              example: example@gmail.com
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: Email sent successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      token:
 *                                          type: string
 *                                          example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MzA4NWVkMzg4NzU5MzkzZDlmZGZiYjYiLCJpYXQiOjE2NjIzNTk4MjgsImV4cCI6MTY2MjYxOTAyOH0.DcMItBe3QqqC3MNp6W5DJd_FJXQ1P0SNm4_x4jyLd90   
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________________Put method___________________
/**
 * @swagger
 * /admin/new/password//{token}:
 *  put:
 *      summary: update Your responseData
 *      tags: [user]
 *      description: update page
 *      parameters:
 *        - in: path
 *          name: token
 *          required: true
 *          description: Redirect to entered page number
 *          schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties: 
 *                          password:
 *                              type: string
 *                              example: Password@123
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: Password changed successfully
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */